import React, { Component } from 'react'
import Content from './Content'
import Footer from './Footer'
import Header from './Header'
import Navbar from './Navbar'

export default class Hw_layour extends Component {
  render() {
    return (
      <>
      <Navbar />

      <div className='row justify-content-center'>
        <div className='col-8'> <Header /> </div>
      </div>
      <div className='row justify-content-center'>
        <div className='col-8'> <Content /> </div>
      </div>

      <Footer />
      
      </>
      
    )
  }
}
