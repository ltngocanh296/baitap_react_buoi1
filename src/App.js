import logo from './logo.svg';
import './App.css';

import BaiTapThucHanhLayout from './BaiTapThucHanhLayout/BaiTapThucHanhLayout.js';

function App() {
  return (
    <div className="App">
      <BaiTapThucHanhLayout />
    </div>
  );
}
export default App;
